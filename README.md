# CMS-Garden Talk

Wie sich die OS-CMS zusammen taten um für Open Source zu kämpfen.

## Präsentation erzeugen

Mit Pandoc wird das HTML für Revealjs generiert.

Zum erzeugen kann das Makefile verwendet werden.

    make html

Es sollte eine aktuelle slides.html im gleichen Verzeichnis erzeut werden.

## Präsensentation ausführen

Mit einen Browser die slides.html aufgerufen.

### Steuerung

- F: Vollbild
- ESC: Vollbild beenden
- S: Presenter-Fenster öffnen (mit Vorschau und timer)
- B: Blank screen
- O: Slide overview

## Installing

Clone the repro like:

```
git clone --recursive https://mrtango@bitbucket.org/derico/cmsgarden-talk-clt2017.git
```

of if you already cloned it, initialize the subrepro like:

```
git submodule init
git submodule update
```

#### NOTE:

There are some symlink for js, lib and plugin, if they are not existing or not working, you should create them by your self.

```
js -> reveal.js/js
lib -> reveal.js/lib
plugin -> reveal.js/plugin/
```

### Requirements

We write Markdown for the content of the slides
 and use Pandoc to generate the Revealjs (HTML5/JS) based presentation.

http://pandoc.org/installing.html
