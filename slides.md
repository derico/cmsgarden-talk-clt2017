% Wie sich die OS-CMS zusammentaten um für OS zu kämpfen
% Maik Derstappen - Derico - md@derico.de
% 12.03.2017

---

# Open Source {data-background-image=background_garten.jpg data-background-position=bottom}

## Web-Content-Management

![](cmsgarden.png)

---

# Wie sich die OS-CMS zusammentaten um für OS zu kämpfen {data-background-image=background_garten.jpg data-background-position=bottom}


---

# Am Anfang war es nur eine Idee {data-background-image=background_garten.jpg data-background-position=bottom}

Wir wollten einen sichtbaren Stand auf der CeBIT, auf der sich mehrere freie Content Management Systeme präsentieren können.


---

# Warum? {data-background-image=background_garten.jpg data-background-position=bottom}

Weil dort bisher kaum freie Systeme zu finden sind.
Und dort nach wie vor viele Entscheider nach Lösungen suchen.

---

# Was wir wollten? {data-background-image=background_garten.jpg data-background-position=bottom}

---

# Einen großen CeBIT-Stand {data-background-image=background_garten.jpg data-background-position=bottom}

>- den man gut wahr nimmt
>- auf dem alle Systeme Platz finden, um sich zu präsentieren

---

# 100 m² sollten reichen {data-background-image=background_garten.jpg data-background-position=bottom}

---

# Kein Branding eines großen Sponsors am Stand {data-background-image=background_garten.jpg data-background-position=bottom}

## CMS-Garden und CMS-Communities sollen im Fokus stehen

---

# Was noch? {data-background-image=background_garten.jpg data-background-position=bottom}

>- professionelle Aufmachung
>- gutes Marketingmaterial
>- solide Technik für die Live-Demos
>- die Kosten für Community-Vertreter gering halten

# Uh, das wird nicht billig {data-background-image=background_garten.jpg data-background-position=bottom}

---

# Geschätzte Kosten {data-background-image=background_garten.jpg data-background-position=bottom}

## ca 100 000 EUR

---

# Wie haben wir das geschafft? {data-background-image=background_garten.jpg data-background-position=bottom}

---

# Wir haben unsere Kräfte gebündelt {data-background-image=background_garten.jpg data-background-position=bottom}

## Gemeinsam erreichen wir mehr

---

# Im ersten Jahr {data-background-image=background_garten.jpg data-background-position=bottom}

## 15 CM-Systeme am Stand

---

# Finanzbedarf je System {data-background-image=background_garten.jpg data-background-position=bottom}

## weniger als 10 000 EUR im Schnitt

---

# Jeder bringt sich ein {data-background-image=background_garten.jpg data-background-position=bottom}

---

# nach seinen Möglichkeiten {data-background-image=background_garten.jpg data-background-position=bottom}

---

# einer für alle, alle für einen {data-background-image=background_garten.jpg data-background-position=bottom}

---

# starke Partner {data-background-image=background_garten.jpg data-background-position=bottom}

## Wir hatten sehr früh einen starken Partner

---

# Großen Dank {data-background-image=background_garten.jpg data-background-position=bottom}

dafür an den

## Open Source Park / Pluspol GmbH

---

# CMS-Gartenfibel {data-background-image=background_garten.jpg data-background-position=bottom}

![10 000 Exemplare](Gartenfibel-2013-1.jpg)

---

# CMS-Gartenfibel {data-background-image=background_garten.jpg data-background-position=bottom}

>- Infos zu Open Source
>- Redaktionelle neutrale CMS-Beschreibungen
>- CMS-Matrix
>- Service-Verzeichnis

---

# Budget aufgestellt durch {data-background-image=background_garten.jpg data-background-position=bottom}

>- Sponsoring der einzelnen CMS-Vereine
>- Anzeigen in der CMS-Gartenfibel
>- Service-Register-Einträge in der CMS-Gartenfibel

---

# Unterbringung der CMS-Vertreter {data-background-image=background_garten.jpg data-background-position=bottom}

## Jugendherberge in Hildesheim

>- günstig
>- Gemeinschaftsraum + Küche
>- Verpflegung selbst organisert

---

# Viel besser als Hotel {data-background-image=background_garten.jpg data-background-position=bottom}

- viele Gespräche bis tief in die Nacht
- gutes Bier
- großartiges Essen, Dank Drupal Chefkoch

---

# final aufgestelltes Budget {data-background-image=background_garten.jpg data-background-position=bottom}

## ca 85 000 EUR

---

# Wir haben es geschafft {data-background-image=cms-garden-2013.jpg}


---

# final Aufgewendet {data-background-image=background_garten.jpg data-background-position=bottom}

## ca 75 000 EUR

---

# Zahlen {data-background-image=background_garten.jpg data-background-position=bottom}

>- 90 m² Standfläche
>- ca 9 000 Besucher am Stand
>- ca 5000 Fibeln verteilt
>- 105 CMS-Vertreter über die Woche verteilt

---

# Wir fallen auf {data-background-image=gärtner.jpg}

---

# Auch in der Presse {data-background-image=presse.jpg}

---

# CeBIT 2014 Fakten {data-background-image=background_garten.jpg data-background-position=bottom}

>- 120 m² Standfläche
>- ca 10 000 Besucher am Stand
>- ca 6000 Fibeln verteilt
>- ca 70 CMS-Vertreter über die Woche verteilt

---

# CeBIT 2014 {data-background-image=cebit-2014-ganzer-stand03_croped.jpg}


---

# Das Team {data-background-image=cebit-2014-team.jpg}

---

# CMS-Garden-Award {data-background-image=background_garten.jpg}

![Open-Source-Sustainability-Award](award.jpg)

---

# Nachhaltigkeit {data-background-image=cebit-2014-team.jpg}

CMS-Garden setzt sich für Nachhaltigkeit ein und fördert den Einsatz und die Verbreitung von Open Source CMS


---

# Weitere Veranstaltungen {data-background-image=background_garten.jpg data-background-position=bottom}

Auf denen wir waren:

- Internet World, München
- Chemnitzer Linuxtage
- Linuxtag, Berlin
- WorldHostingDays, Rust
- FrOSCon, Sankt Augustin/Bonn
- DMS-EXPO, Stuttgart
- OpenRheinRuhr

---

# CMS-Garden-UnConference {data-background-image=background_garten.jpg data-background-position=bottom}

- findet jährlich statt (November)
- über den Telerrand schauen
- sich austauschen
- frische Ideen
- frische Motivation

---

# Aktuell im CMS-Garden {data-background-image=background_garten.jpg data-background-position=bottom}

![](systeme.png)

---

# Seid dabei {data-background-image=background_garten.jpg data-background-position=bottom}

---

# Ihr macht Open Source Web-CMS? {data-background-image=background_garten.jpg data-background-position=bottom}

dann kommt in den CMS-Garden!

---

# Infos auf unser Website {data-background-image=background_garten.jpg data-background-position=bottom}

cms-garden.org

---

# Auf dem laufenden bleiben {data-background-image=background_garten.jpg data-background-position=bottom}

Meldet Euch auf community.cms-garden.org an!

---

# Ihr macht andere coole OS-Sachen {data-background-image=background_garten.jpg data-background-position=bottom}

dann macht es uns nach und vereinigt Euch!

---

# Vielen Dank {data-background-image=background_garten.jpg data-background-position=bottom}

## Maik Derstappen - Derico - md@derico.de

![](cmsgarden.png)
